export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'project-nuxtjs',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Workshop: NextJS vs NuxtJS' },
      { name: 'format-detection', content: 'telephone=no' },
      { name: 'keywords', content : 'workshop' },
      { name: "subject", content: "NextJS vs NuxtJS" },
      { name: "copyright", content: "Amitgroup" },
      { name: "author", content: "Nguyen Cao Anh Minh - Huynh Trung Hien" },
      { property: "og:title", content: "Workshop: NextJS vs NuxtJS" },
      { property: "og:description", content: "Workshop: NextJS vs NuxtJS" },
      { property: "og:url", content: "https://nextjs-workshop-team3.vercel.app/" },
      { property: "og:site_name", content: "Workshop: NextJS vs NuxtJS" },
      { property: "og:type", content: "website" },
      { property: "og:image", content: "https://i.pinimg.com/564x/44/e3/72/44e372f2e74d3f3e0cf247adda6b7b48.jpg", key: "ogimage" },
      { name: "theme-color", content: "#317EFB" }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'ant-design-vue/dist/antd.css',
    '@/assets/scss/index.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/antd-ui'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },
  loading: {
    color: 'red',
    height: '4px',
    continuous: true,
    duration: 1000
  }
}
